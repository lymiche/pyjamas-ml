.. _installation:

.. _anaconda: https://www.anaconda.com/
.. _ipywidgets: https://ipywidgets.readthedocs.io/en/latest/user_install.html
.. _Jupyter: https://jupyter.org/
.. _miniconda: https://docs.conda.io/en/latest/miniconda.html
.. _Nodejs: https://nodejs.org/
.. _pip: https://pip.pypa.io
.. _PyJAMAS: https://bitbucket.org/rfg_lab/pyjamas/src/master/
.. _Python: https://www.python.org/downloads/
.. _Shapely: https://www.lfd.uci.edu/~gohlke/pythonlibs/#shapely
.. _venv: https://docs.python.org/3/library/venv.html
.. _virtualenv: https://virtualenv.pypa.io/

============
Installation
============

#. Install Python_ 3.8 or 3.9.

#. Download and install PyJAMAS_ by opening a terminal and typing:

   .. code-block:: bash

    $ python3 -m pip install --no-cache-dir -U pyjamas-rfglab

   pip_ is a package manager for Python. pip_ is typically installed with Python_, but you can also install pip_ following these `instructions <https://pip.pypa.io/en/stable/installing/>`_.

   After installing pip_, it is good practice to update it. To update pip_, open a terminal and type:

   .. code-block:: bash

    $ python3 –m pip install –U pip

   substituting **python3** with the name of the Python_ executable (typically **python3** in MacOS and Linux, and **py** in Windows).

   **NOTES FOR APPLE M1 USERS**

   PyJAMAS_ can take advantage of the new Apple M1 processors and their GPU capabilities. However, this requires a different installation process using miniconda_.

   First, install miniconda_. In contrast to pip, miniconda_ can install packages running natively on the M1 architecture.

   We recommend creating and activating a virtual environment as indicated below.

   Within your miniconda_ virtual environment, install Tensorflow:

   .. code-block:: bash

    $ conda install -c apple tensorflow

   Then install Jupyter_:

   .. code-block:: bash

    $ conda install -c conda-forge jupyter jupyterlab

   And finally install the rest of packages necessary for PyJAMAS_ to run:

   .. code-block:: bash

    $ conda install openblas pyqt joblib lxml pandas scikit-image scikit-learn seaborn shapely opencv

   The last step is to install PyJAMAS_:

   .. code-block:: bash

    $ python -m pip install --no-deps pyjamas-rfglab

   *GPU support on the Apple M1*

   PyJAMAS_ supports the use of the GPU on the M1 chip. To do this, install tensorflow-metal (version 0.2 if you are on Big Sur - OSX 11, version 0.3 if you are on Monterey - OSX 12). For example, on Big Sur:

   .. code-block:: bash

    $ python -m pip install tensorflow-metal==0.2

   **NOTES FOR WINDOWS USERS**

   PyJAMAS_ uses the package Shapely_ to represent geometrical objects, such as points or polygons. Shapely_ is automatically installed with PyJAMAS_ in MacOS and Linux. However, the installation needs to be done manually in Windows, after installing Python_ and pip_, and **before** installing PyJAMAS_.

   To install Shapely_, download the appropriate version from this site: https://www.lfd.uci.edu/~gohlke/pythonlibs/#shapely. For example, use Shapely‑1.6.4.post2‑cp38‑cp38m‑win_amd64.whl for a 64-bit machine running Python 3.8.

   Open a terminal and navigate to the folder that contains the downloaded .whl file using the cd command.

   Complete the installation of Shapely by typing:

   .. code-block:: bash

    $ python -m pip install Shapely‑1.6.4.post2‑cp38‑cp38m‑win_amd64.whl

   substituting the downloaded file name as appropriate.

   *GPU support under Windows*

   PyJAMAS_ supports the use of CUDA-enabled GPUs in Windows. Please, check here (https://tensorflow.org/install/gpu) for instructions on how to configure your system. Briefly:

   #. Download and install the NVIDIA GPU drivers (https://www.nvidia.com/drivers).

   #. Download and install the CUDA Toolkit (https://developer.nvidia.com/cuda-toolkit-archive).

   #. Download and install the cuDNN SDK (https://developer.nvidia.com/cudnn and https://docs.nvidia.com/deeplearning/cudnn/install-guide/index.html).

   **Known problems**: CUDA and cuDNN are picky with the version of each other that they talk to. If PyJAMAS_ displays an error that cusolver64_10.dll is not found:

   #. Go to the folder C:\\Program Files\\NVIDIA GPU Computing Toolkit\\CUDA\\V11.2\\bin (replacing V11.2 by whichever version you installed).
   #. Create a copy of the file cusolver64_11.dll.
   #. Rename the copy as cusolver64_10.dll.

   **NOTES FOR LINUX USERS**

   In Linux systems, you will need permissions to install PyJAMAS_ globally. To restrict the PyJAMAS_ installation to the current user, install it with:

   .. code-block:: bash

    $ python3 –m pip install --user --no-cache-dir -U pyjamas-rfglab

#. The analysis of image batches in PyJAMAS_ can generate interactive Jupyter_ notebooks. Interactivity in Jupyter_ notebooks relies on ipywidgets_, a package installed with PyJAMAS_. Please, check the ipywidgets_ documentation if you have issues with interactivity in notebooks (e.g. there are no interactive features). Most often the following steps are sufficient to fix any issues:

    a. Download and install the Nodejs_ JavaScript runtime.

    b. Open a new terminal and execute the following command for JupyterLab:

        .. code-block:: bash

            $ jupyter labextension install @jupyter-widgets/jupyterlab-manager

      or this one for Jupyter Notebook:

        .. code-block:: bash

            $ jupyter nbextension enable --py widgetsnbextension

    c. Reopen your Jupyter_ server.

#. To run PyJAMAS_, open a terminal and type:

   .. code-block:: bash

    $ pyjamas

#. The code for PyJAMAS_ can be found in the PyJAMAS_ folder under the Python_ site packages (e.g. /usr/local/lib/python3.8/site-packages in MacOS). The location of the source code is important to extend PyJAMAS_ using `plugins <plugins.html>`_. Alternatively, you can download the PyJAMAS_ source code from: https://bitbucket.org/rfg_lab/pyjamas/src/master/.

#. PyJAMAS_ can be run from the source code by opening a terminal, navigating to the folder that contains the code, and typing:

   .. code-block:: bash

    $ python3 -m pyjamas.pjscore

Using virtual environments
==========================

Virtual environments allow isolation of Python_ packages, preventing interference and incompatibilities between different package dependencies and versions thereof.

PyJAMAS_ can be used inside a virtual environment. Here we describe how to use three different types of virtual environments: venv_, virtualenv_ and conda_.

*venv*
************

venv_ is included in the Python_ standard library. Therefore, venv_ is the easiest way to create and manage virtual environments.

In a terminal or a command prompt, change to the desired folder and create a virtual environment with:

.. code-block:: bash

 $ python -m venv envpyjamas

substituting *python* with the name of the Python_ interpreter to use in the virtual environment (e.g. *py* in Windows), and *envpyjamas* with the name of the folder in which the virtual environment will be created.

Next, activate the environment with:

.. code-block:: bash

 $ source envpyjamas/bin/activate

or in Windows:

.. code-block:: batch

 $ .\envpyjamas\Scripts\activate

Now you may proceed with the download and installation of PyJAMAS_ as above, using *python* rather than *python3* or *py* as the name of the Python_ interpreter.

You can deactivate the virtual environment at any time with:

.. code-block:: bash

 $ deactivate

*virtualenv*
************

Download and install virtualenv_. In a terminal or a command prompt, change to the desired folder and create a virtual environment with:

.. code-block:: bash

 $ virtualenv -p python3.8 envpyjamas

substituting *python3.8* with the Python_ interpreter to use in the virtual environment (e.g. *py* in Windows), and *envpyjamas* with the name of the folder in which the virtual environment will be created.

Next, activate the environment with:

.. code-block:: bash

 $ source envpyjamas/bin/activate

or in Windows:

.. code-block:: batch

 $ .\envpyjamas\Scripts\activate

Now you may proceed with the download and installation of PyJAMAS_ as above, using *python* rather than *python3* or *py* as the name of the Python_ interpreter.

You can deactivate the virtual environment at any time with:

.. code-block:: bash

 $ deactivate

*conda*
*******

Download and install anaconda_ or miniconda_. In a terminal or an anaconda_ power shell, create a virtual environment with:

.. code-block:: bash

 $ conda create --name envpyjamas python=3.8

substituting *3.8* with the version of the Python_ interpreter that you would like to use, and *envpyjamas* with the name of the virtual environment.

Keep in mind that anaconda_ stores virtual environments within the folder that contains the anaconda_ distribution. You can find the location of your virtual environment with:

.. code-block:: bash

 $ conda info --envs


Next, activate the environment with:

.. code-block:: bash

 $ conda activate envpyjamas

Now you may proceed with the download and installation of PyJAMAS_ as above, using *python* rather than *python3* as the name of the Python_ interpreter.

You can deactivate the virtual environment at any time with:

.. code-block:: bash

 $ conda deactivate
